<?php

spl_autoload_register(function($c)
{
	@include_once strtr($c, '\\_', '//') . '.php';
});
set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . '/Source');

use \Suin\RSSWriter\Feed;
use \Suin\RSSWriter\Channel;
use \Suin\RSSWriter\Item;

require('./phpQuery/phpQuery.php');

$fp = fopen('tgc.csv', 'r');
while(($data = fgetcsv($fp, 70)) !== FALSE)
	$state[$data[0]] = $data[1];
fclose($fp);

if (! isset($state[$username]))
	$state[$username] = 1;

$username = isset($_GET['username']) ? $_GET['username'] : NULL;
$offset   = isset($_GET['offset'])   ? $_GET['offset']   : $state[$username];
$count    = isset($_GET['count'])    ? $_GET['count']    : 25;
$limit    = isset($_GET['limit'])    ? $_GET['limit']    : 10;

$debug = "";

if (!$username)
	return;

$ch = curl_init("https://t.me/${username}");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$page = curl_exec($ch);
if ($page == FALSE)
	return;

$doc  = phpQuery::newDocument($page);

$chan_title = pq('div.tgme_page_title')->text();
$chan_desc  = pq('div.tgme_page_description')->wrapInner('<p></p>')->html();
$header_img = pq('img.tgme_page_photo_image')->attr('src');

$feed = new Feed();
$channel = new Channel();

$channel
	->title("{$chan_title}")
	->description("<img src='{$header_img}'></img><br/{$chan_desc}")
	->url("https://t.me/{$username}")
	->pubdate(time())
	->lastBuildDate(time())
	->ttl(30)
	->appendTo($feed);

$lastok = $offset;
$errcnt = 0;

for ($i = $offset; $i < $offset+$count; $i++)
{
	curl_setopt($ch, CURLOPT_URL, "https://t.me/${username}/{$i}?embed=1&single=1");
	$page = curl_exec($ch);
	if ($page == FALSE)
	{
		$i--;
		continue;
	}
	
	$doc = phpQuery::newDocument($page);


	if (pq('div')->hasClass('tgme_widget_message_error'))
	{
		if (strpos(pq('.tgme_widget_message_error')->text(), "Post not found") !== FALSE)
		{
			$debug .= "<!-- Err post #{$i} not found ({$errcnt}/{$limit} in chain) -->\n";
			$errcnt++;

			if ($errcnt > $limit)
				break;
			continue;
		} else if (strpos(pq('.tgme_widget_message_error')->text(), "Channel with username") !== FALSE)
		{
			$debug .= "<!-- Err post #{$i}, retrying in 1 second. -->\n";
			sleep(5);
			$i--;
			continue;
		} else
		{
			$debug .= "<!-- Uh huh? -->\n";
		}
			continue;
	} else
	{
		$errcnt = 0;
		$lastok = $i;
	}

	$item_title = mb_strimwidth(strtok(pq('div.tgme_widget_message_text')->text(),"\n"), 0, 70, "…");

	$item_body = '';

	if (pq('div')->hasClass('tgme_widget_message_photo'))
	{
		$body_img = pq('a.tgme_widget_message_photo_wrap')->attr('style');
		$body_img = preg_replace('`.*background-image:url\(\'(.+?)\'\).*`', '<img src="$1" /><br>', $body_img);
		$item_body .= $body_img;
	}

	if (pq('div')->hasClass('tgme_widget_message_video_wrap'))
	{
		pq('div.link_preview_video_wrap')->removeAttr('style');
		pq('video')->removeAttr('id');
		pq('video')->attr('width', '100%');
		//pq('video')->attr('height', '650vh');
		pq('video')->attr('controls', "true");
		pq('video')->removeAttr('class');
		$item_body .= pq('div.tgme_widget_message_video_wrap')->wrapInner('<p></p>')->html();
	}

	if (pq('div')->hasClass('tgme_widget_message_roundvideo_wrap'))
	{
		pq('div.link_preview_roundvideo_wrap')->removeAttr('style');
		pq('video')->removeAttr('id');
		pq('video')->removeAttr('width');
		pq('video')->removeAttr('height');
		pq('video')->removeAttr('muted');
		pq('video')->attr('controls', "true");
		pq('video')->removeAttr('class');
		$item_body .= pq('div.tgme_widget_message_roundvideo_wrap')->wrapInner('<p></p>')->html();
	}

	if (pq('a')->hasClass('tgme_widget_message_reply'))
		$item_body .= pq('a.tgme_widget_message_reply')->wrapInner('<blockquote></blockquote>')->html();

	//////////////////////
	pq('.emoji')->removeAttr('style');
	foreach(pq('')->find('.emoji') as $elem)
		pq($elem)->replaceWith(pq($elem)->text());
	$item_body .= pq('div.tgme_widget_message_bubble > div.tgme_widget_message_text')->wrapInner('<p></p>')->html();
	//////////////////////

	if (pq('div')->hasClass('tgme_widget_message_sticker_wrap'))
	{
		$sticker_img = pq('i.tgme_widget_message_sticker')->attr('style');
		$sticker_img = preg_replace('`.*background-image:url\(\'(.+?)\'\).*`', '<img src="$1" /><br>', $sticker_img);
		$item_body .= $sticker_img;
	}

	if (pq('a')->hasClass('tgme_widget_message_link_preview'))
	{
		$pghref = pq('div.link_preview_site_name')->text();
		if (strstr($pghref, 'Telegraph'))
		{
			$ch = curl_init(pq('a.tgme_widget_message_link_preview')->attr('href'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$tmppage = curl_exec($ch);
			$tmppage = str_replace('<head>', '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>', $tmppage);
			
			$tmpdoc = new DOMDocument('1.0');
			@$tmpdoc->loadHTML($tmppage);

			if ($tmpdoc->getElementById('tl_article_header')->lastChild != null)
				$tmpdoc->getElementById('tl_article_header')->removeChild($tmpdoc->getElementById('tl_article_header')->lastChild);
			if ($tmpdoc->getElementById('_tl_editor')->childNodes != null)
				$tmpdoc->getElementById('_tl_editor')->removeChild($tmpdoc->getElementById('_tl_editor')->childNodes->item(0));

			
			foreach ($tmpdoc->getElementsByTagName('img') as $image)
				if ($image->getAttribute('src')[0] == '/')
					$image->setAttribute('src', 'https://telegra.ph' . $image->getAttribute('src'));
			
			$item_body .= '<hr/>' . $tmpdoc->saveHTML($tmpdoc->getElementsByTagName('header')->item(0)) . $tmpdoc->saveHTML($tmpdoc->getElementsByTagName('article')->item(0));;

		}
		else
		{
			if (pq('div')->hasClass('link_preview_video_wrap'))
				pq('div.link_preview_video_wrap')->removeAttr('style');
	
			if (pq('i')->hasClass('link_preview_image'))
			{
				$preview_img = pq('i.link_preview_image')->attr('style');
				$preview_img = preg_replace('`.*background-image:url\(\'(.+?)\'\).*`', '<img src="$1" /><br>', $preview_img);
				$item_body .= $preview_img;
			}
	
			if (pq('div')->hasClass('link_preview_embed_wrap'))
			{
				pq('div.link_preview_embed_wrap')->removeAttr('style');
				pq('div.link_preview_embed_wrap > iframe')->attr('width', '640');
				pq('div.link_preview_embed_wrap > iframe')->attr('height', '360');
			}
	
			$item_body .= pq('a.tgme_widget_message_link_preview')->wrapInner('<blockquote></blockquote>')->html();
		}
	}

	if (pq('div')->hasClass('tgme_widget_message_document'))
		$item_body .= pq('div.tgme_widget_message_document_title')->wrapInner('<blockquote></blockquote>')->html();

	if (pq('*')->hasClass('tgme_widget_message_forwarded_from_name'))
		$item_author = pq('.tgme_widget_message_forwarded_from_name')->text();
	else
		$item_author = pq('.tgme_widget_message_owner_name')->text();

	if (pq('*')->hasClass('tgme_widget_message_forwarded_from_author'))
		$item_author .= ' (' . pq('.tgme_widget_message_forwarded_from_author')->text() . ')';

	if (pq('*')->hasClass('tgme_widget_message_forwarded_from_name'))
		$item_body = pq('.tgme_widget_message_forwarded_from')->html() . '<blockquote>' . $item_body . '</blockquote>';
		

	$dateprep  = pq('.tgme_widget_message_date time')->attr('datetime');
	$debug .= "<!-- post #{$i} {$dateprep} -->\n";
	$item_date = strtotime($dateprep);
	if ($item_title == "")
		$item_title = "Untitled post from " . pq('.tgme_widget_message_date')->text();

	$item = new Item();
	$item
		->title($item_title)
		->author($item_author)
		->description("$item_body")
		->url("https://t.me/${username}/{$i}")
		->pubDate($item_date)
		->guid("${username}/{$i}", false)
		->appendTo($channel);

	preg_match_all('`#\K([^[:blank:],.<"\']+)`', $item_body, $matches);

	foreach ($matches[0] as $match)
		$item->category($match);

}

$state[$username] = $lastok;

$fp = fopen('tgc.csv', 'r');
while(($data = fgetcsv($fp, 70)) !== FALSE)
	if ($state[$data[0]] != $data[1] && $data[0] != $username)
		$state[$data[0]] = $data[1];
fclose($fp);

$fp = fopen('tgc.csv', 'w');
foreach($state as $key => $line)
	fputcsv($fp, array($key, $line));
fclose($fp);

echo $feed;
//echo $debug;
