# TGC-RSS

Telegram channel RSS adapter.

*New!* Now supports inline display of Telegra.ph articles! 

## Requirements

* php
* php-dom
* php-curl

## Configuration

* Edit proxy settings in `tgc.php`
* Find the channel you would like to subscribe to on Telegram and copy the link to the latest post
* The link will contain the post ID. Add a new line in `tgc.csv` in the form of: `*channel_name*,*(post_id-10)*`.
* Create a shell script which would call `tgc.php *channel_name*` (see `tarabanda.sh` for example) *[probably only needed for Liferea as it cannot properly pass the command line arguments directly to tgc.php?]*
* Subscribe to the created shell script


### Notice:

The file `tgc.original.php` is the original script by gougou intended for use on an HTTP server.
The file `tgc.php` is modified by Akasaka to work with Liferea as a command line script, and use a local Tor instance as an anti-censorship proxy.

----

Script by gougou.
Edited for proxy and local installation by akasaka @ Genjitsu Labs, 2018.
Uses RSSWriter by Suin and phpQuery by Tobiasz Cudnik.
