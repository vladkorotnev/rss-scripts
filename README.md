# RSS-Scripts

A small collection of scripts to read various websites in Liferea or other RSS readers supporting local command feed generation.

Currently supported:

* pikabu.ru -- see `pikarss` directory.
* Telegram channels -- see `tgc` directory.

For configuration details check out `README.md` in respective subdirectories.

----

gougou & Genjitsu Labs, 2018.
