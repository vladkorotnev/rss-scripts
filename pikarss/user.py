#!/usr/bin/python
#-*- coding: utf-8 -*-

LINK_TO_COMMENTS=True

from cookie import *
from PyRSS2Gen import *
from rssextz import *
from story import *
from bs4 import BeautifulSoup
import requests, sys
from dateutil import *

if len(sys.argv) < 2:
	sys.exit()

username = sys.argv[1]

headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    }
)
r = requests.get('https://pikabu.ru/@'+username, cookies=COOKIE, headers=headers)
if r.status_code != 200:
	print "error"
	sys.exit()
	
soup = BeautifulSoup(r.content, "html.parser")
stories = soup.find_all("article", "story")

for_rss = []

for c in stories:
	if storyIsAd(c):
		continue
	rss_reply = MediaRSS2(title = findStoryTitle(c),
						link = findStoryLink(c)+ ('#comments' if LINK_TO_COMMENTS else ''),
						description = str(findStoryContentBlock(c)).decode('utf-8'),
						guid = Guid(findStoryLink(c)),
						pubDate = findStoryDate(c).astimezone(tz.tzutc()),
						author = findStoryAuthor(c),
						categories = findStoryTags(c))
	for_rss.append(rss_reply)
	
rss = RSS2(title = username+"@Pikabu",link = "https://pikabu.ru/@"+username,description = "Pikabu User "+username+" Feed",lastBuildDate = datetime.datetime.now(),items = for_rss)
rss.write_xml(sys.stdout)
