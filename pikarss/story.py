#-*- coding: utf-8 -*-

# Some pikabu story selectors

def findStoryContentBlock(story):
	return story.find("div","story__content")

def findStoryTitle(story):
	return story.find("h2","story__title").text.strip()

def findStoryLink(story):
	return story.find("a","story__title-link").attrs["href"].strip() 

def findStoryAuthor(story):
	return story.find("a","user__nick").text.strip()
	
def findStoryTags(story):
	tags = []
	for t in story.find_all("a","tags__tag"):
		tags.append(t.text.strip())
	return tags

def storyIsAd(story):
	return ( (not (story.find("a", "story__sponsor")) is None) or (story.find("time","story__datetime") is None) )

from dateutil.parser import parse
from dateutil import *
def findStoryDate(story):
	return parse(story.find("time","story__datetime").attrs["datetime"].strip()).astimezone(tz.tzlocal())

import datetime
def storyIsTooOld(story, maxage):
	then = findStoryDate(story)
	now = datetime.datetime.now(then.tzinfo)
	dt = now - then
	return (dt.days >= maxage)
