# PikaRSS

Pikabu.ru RSS feed generator.

## Features

* Generate an RSS feed from your subscriptions
* Generate an RSS feed of your inbox

## Prerequisites

* Python 2.x
* PyRSS2Gen
* requests
* BeautifulSoup (bs4)

## Configuration

1. Log in to Pikabu.ru using your web browser
2. While logged in, copy the cookie using Web Inspector (i.e. by typing `document.cookie` in the JavaScript console)
3. Break down the cookie into a dictionary in `cookie.py` (template included)
4. Subscribe to `feed.py` for your subscriptions
5. Subscribe to `inbox.py` for your inbox

----

Genjitsu Labs, 2018
