import PyRSS2Gen

class NoOutput:
    def __init__(self):
        pass
    def publish(self, handler):
        pass
 
class MediaRSS2(PyRSS2Gen.RSSItem):
    def __init__(self, **kwargs):
        PyRSS2Gen.RSSItem.__init__(self, **kwargs)
 
    def publish(self, handler):
        self.do_not_autooutput_description = self.description
        self.description = NoOutput() # This disables the Py2GenRSS "Automatic" output of the description, which would be escaped.
        PyRSS2Gen.RSSItem.publish(self, handler)
 
    def publish_extensions(self, handler):
        handler._write(u'<%s><![CDATA[%s]]></%s>' % ("description", self.do_not_autooutput_description, "description"))
        
