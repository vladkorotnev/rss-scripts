#!/usr/bin/python
#-*- coding: utf-8 -*-

from cookie import *
import datetime
from PyRSS2Gen import *
from bs4 import BeautifulSoup
import requests, os, pdb, sys
from dateutil.parser import parse
from dateutil import *

headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    }
)
r = requests.get('https://pikabu.ru/answers', cookies=COOKIE, headers=headers)
if r.status_code != 200:
	print "error"
	os.exit()

soup = BeautifulSoup(r.content, "html.parser")
replies = soup.find_all("div", "comments")

def getCommentText(rr):
	return rr.find('div','comments__main').find('div','comment__content').text.strip()

def getCommentAuthor(rr):
	return rr.find('div','comments__main').find('div','comment__user').attrs["data-name"].strip()

def getCommentTs(rr):
	return parse(rr.find('div','comments__main').find('time','comment__datetime').attrs['datetime'].strip())

def getCommentLink(rr):
	return rr.find('div','comments__main').find('a','comment__tool').attrs['href'].strip()
	
def getCommentParentTitle(rr):
	return rr.find('div','comments__title').find('a').text.strip()

for_rss = []

for c in replies:
	rss_reply = RSSItem(title = ("[Reply] "+getCommentParentTitle(c)),
						link = getCommentLink(c),
						author = getCommentAuthor(c),
						description = getCommentText(c),
						guid = Guid(getCommentLink(c)),
						pubDate = getCommentTs(c).astimezone(tz.tzutc()))
	for_rss.append(rss_reply)
	
rss = RSS2(title = "Pikabu Inbox",link = "https://old.pikabu.ru/freshitems.php",description = "Pikabu incoming comments",lastBuildDate = datetime.datetime.now(),items = for_rss)
rss.write_xml(sys.stdout)
